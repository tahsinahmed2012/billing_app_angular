import { LocationDetail } from './location-detail';

export class LocationLocationDetail {
  id: string;
  name: string;
  number_of_children: string;
  location_details: LocationDetail[];
}
