export class Location {
  id: string;
  name: string;
  number_of_children: string;
  location_id: string;
  location: Location;
  constructor(name: string = '') {
    this.name = name;
  }
}
