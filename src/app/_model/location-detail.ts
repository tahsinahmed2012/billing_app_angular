export class LocationDetail {
  id: string;
  name: string;
  location_detail_id: string;
  location_id: string;
  constructor(name: string = '') {
    this.name = name;
  }
}
