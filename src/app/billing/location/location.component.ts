import { Component, OnInit } from '@angular/core';
import { LocationService } from '../service/location.service';
import { Location } from '../../_model/location';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  locations: Location;
  printLocations: any[] = [];
  constructor(private locationService: LocationService) {}

  ngOnInit() {
    this.locationService.index().subscribe(locations => {
      this.locations = locations;
      this.setLocationHierarchy();
    });
  }

  setLocationHierarchy() {
    while (this.locations !== null) {
      this.printLocations.push({
        id: this.locations.id,
        name: this.locations.name
      });
      this.locations = this.locations.location;
    }
  }

}
