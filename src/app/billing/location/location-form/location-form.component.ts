import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LocationService } from '../../service/address-setup.service';
import { Location } from '../../../_model/location';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss']
})
export class LocationFormComponent implements OnInit {

  locationForm: FormGroup;
  editLocation: Location;
  id: number;
  constructor(
    private addressSetupService: LocationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.editLocation = new Location();
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.addressSetupService.show(this.id).subscribe(
          data => {
            this.editLocation = data;
          },
          error => {
            console.log(error);
          }
        );
      }
    });
    this.formInit();
  }

  private formInit() {
    this.locationForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  onSubmit() {
    const location = new Location(this.locationForm.value['name']);
    if (this.id) {
      this.addressSetupService.update(this.id, location).subscribe(
        response => {
          this.router.navigate(['../address-setup']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.addressSetupService
        .store(location)
        .subscribe(response => console.log(response));
    }
  }

}
