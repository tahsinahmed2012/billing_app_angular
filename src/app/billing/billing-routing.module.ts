import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomerFormComponent} from './customer-form/customer-form.component';
import {LocationComponent} from './location/location.component';
import {LocationFormComponent} from './location/location-form/location-form.component';
import {LocationDetailFormComponent} from './location-detail-form/location-detail-form.component';

const routes: Routes = [
  { path: 'customer/create', component: CustomerFormComponent },
  { path: 'location', component: LocationComponent },
  { path: 'location/create', component: LocationFormComponent },
  { path: 'location/:id/edit', component: LocationFormComponent },
  { path: 'location-detail/:id/create', component: LocationDetailFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule { }
