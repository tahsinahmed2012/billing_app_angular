import { TestBed, inject } from '@angular/core/testing';

import { LocationLocationDetailService } from './location-location-detail.service';

describe('LocationLocationDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationLocationDetailService]
    });
  });

  it('should be created', inject([LocationLocationDetailService], (service: LocationLocationDetailService) => {
    expect(service).toBeTruthy();
  }));
});
