import { TestBed, inject } from '@angular/core/testing';

import { LocationDetailService } from './location-detail.service';

describe('LocationDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationDetailService]
    });
  });

  it('should be created', inject([LocationDetailService], (service: LocationDetailService) => {
    expect(service).toBeTruthy();
  }));
});
