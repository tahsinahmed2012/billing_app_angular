import { Injectable } from '@angular/core';
import { Location } from '../../_model/location';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationLocationDetailService {
  constructor(private http: HttpClient) {}
  index(locationInput: any) {
    return this.http.get<Location>(
      '/location/' +
        locationInput.id +
        '/location_detail/' +
        locationInput.parent.id
    );
  }
}
