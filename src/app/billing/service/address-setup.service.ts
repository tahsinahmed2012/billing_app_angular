import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Location } from '../../_model/location';
import { Permission } from '../../_model/permission';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor(private http: HttpClient) {}
  index(): Observable<Location> {
    return this.http.get<Location>('/location');
  }

  store(location: Location) {
    return this.http.post('/location', location);
  }

  update(id: number, location: Location) {
    return this.http.put('/location/' + id, location);
  }

  show(id: number): Observable<Location> {
    return this.http.get<Location>('/location/' + id);
  }
  delete() {}
}
