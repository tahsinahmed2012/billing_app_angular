import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { CustomerTableComponent } from './customer-table/customer-table.component';
import { CardModule } from 'primeng/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { AutoCompleteModule, DropdownModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { TreeModule } from 'primeng/tree';
import { PanelModule } from 'primeng/panel';
import { SplitButtonModule } from 'primeng/primeng';

import { LocationComponent } from './location/location.component';
import { LocationFormComponent } from './location/location-form/location-form.component';
import { LocationDetailFormComponent } from './location-detail-form/location-detail-form.component';

@NgModule({
  imports: [
    CommonModule,
    BillingRoutingModule,
    CardModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DropdownModule,
    CheckboxModule,
    TreeModule,
    PanelModule,
    SplitButtonModule,
    AutoCompleteModule
  ],
  declarations: [
    CustomerFormComponent,
    CustomerTableComponent,
    LocationComponent,
    LocationFormComponent,
    LocationDetailFormComponent
  ]
})
export class BillingModule {}
