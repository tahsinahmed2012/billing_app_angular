import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationDetailFormComponent } from './location-detail-form.component';

describe('LocationDetailFormComponent', () => {
  let component: LocationDetailFormComponent;
  let fixture: ComponentFixture<LocationDetailFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationDetailFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationDetailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
