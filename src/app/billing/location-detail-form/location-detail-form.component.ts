import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LocationService } from '../service/address-setup.service';
import { _ } from 'underscore';
import { LocationDetailService } from '../service/location-detail.service';
import { LocationLocationDetailService } from '../service/location-location-detail.service';
import {Location} from '../../_model/location';

@Component({
  selector: 'app-location-detail-form',
  templateUrl: './location-detail-form.component.html',
  styleUrls: ['./location-detail-form.component.scss']
})
export class LocationDetailFormComponent implements OnInit {
  id: number;
  locations: Location;
  location: Location;
  locationInputs: any[] = [];
  constructor(
    private locationService: LocationService,
    private locationDetailService: LocationDetailService,
    private locationLocationDetailService: LocationLocationDetailService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.locationService.show(this.id).subscribe(location => {
          this.location = location;
          this.locationService.index().subscribe(locations => {
            this.locations = locations;
            this.setLocationInput();
          });
        });
      }
    });
  }
  setLocationInput() {
    while (this.locations !== null && this.locations.id !== this.location.id) {
      this.locationInputs.push({
        id: this.locations.id,
        name: this.locations.name,
        parent: {},
        locationDetails: []
      });
      this.locations = this.locations.location;
    }
  }

  searchLocation(event, index, locationInput) {
    if (!_.isEmpty(locationInput)) {
      this.locationDetailService.index(locationInput).subscribe(response => {
        this.locationInputs[index].locationDetails = response;
      });
    }
  }

  selectLocation(event, index) {
    console.log(this.locationInputs);
    this.locationInputs[index + 1]
      ? (this.locationInputs[index + 1].parent = event)
      : console.log(index);
  }

}
